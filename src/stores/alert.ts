import { reactive, ref } from 'vue'
import { defineStore } from 'pinia'

interface AlertItem {
  show: boolean;
  message: string;
  timeout?: number;
  variant?: string;
}

export const useAlertStore = defineStore('alert', () => {
  const alertItem = reactive({
    show: false,
    message: '',
    timeout: 3000,
    variant: 'success'
  })

  function setAlert(item: AlertItem) {
    alertItem.show = item.show;
    alertItem.message = item.message;
    alertItem.variant = item.variant || 'success';
  }

  return { alertItem, setAlert }
})
