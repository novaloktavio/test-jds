import { ref } from 'vue'
import { defineStore } from 'pinia'

export const usePenerimaBantuanStore = defineStore('penerima-bantuan', () => {
  const items = ref([
    {
      name: 'John Doe',
      nik: '32732371239123',
      kk: '32732371239123',
      age: '28',
      gender: 'Laki Laki',
      province: {
        id: '0',
        name: 'Jawa Barat'
      },
      city: {
        id: '0',
        name: 'Kota Bandung'
      },
      district: {
        id: '0',
        name: 'Rancasari'
      },
      address: 'Jl Jakarta no 23',
      rt: '01',
      rw: '02',
      alasanLainnya: '',
      fotoKtp: null,
      fotoKk: null,
      penghasilan_sebelum: '3000000',
      penghasilan_setelah: '2000000',
      reason: 'Pengurangan gaji',
    }
  ])
  function addNew(item: any) {
    items.value.push(item)
  }

  return { items, addNew }
})
