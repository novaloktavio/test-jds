import axios from 'axios';

// https://www.emsifa.com/api-wilayah-indonesia/api/provinces.json
export const getProvinces = async () => {
  return axios.get(`/provinces.json`);
};

export const getCities = async (id: any) => {
  return axios.get(`/regencies/${id}.json`);
};

export const getDistricts = async (id: any) => {
  return axios.get(`/districts/${id}.json`);
};

