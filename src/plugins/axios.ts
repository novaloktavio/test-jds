import axios from 'axios';
import {BASE_URL} from '@/api';
axios.defaults.baseURL = BASE_URL;

axios.interceptors.request.use(
  (config: any) => {
    const token = localStorage.getItem('auth.token');
    console.log('token', !!token);
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }

    return config;
  },

  (error) => {
    return Promise.reject(error);
  },
);
